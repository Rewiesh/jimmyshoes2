package sr.unasat.DAO;

import sr.unasat.entities.Merchant;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class MerchantDao {

    private EntityManager entityManager;

    public MerchantDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Merchant> retrieveMerchantList() {
        entityManager.getTransaction().begin();

        String jpql = "select m from Merchant m";
        TypedQuery<Merchant> query = entityManager.createQuery(jpql, Merchant.class);
        List<Merchant> merchantList = query.getResultList();
        entityManager.getTransaction().commit();
        return merchantList;
    }
}
