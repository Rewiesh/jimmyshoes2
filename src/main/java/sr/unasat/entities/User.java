package sr.unasat.entities;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @Column
    private String username;
    private String password;
    private String firstname;
    private String second_name;
    private String lastname;
    private String mobilenumber;
    private String telefoonnumber;
//    @OneToOne(mappedBy = "user_types")
    private Integer user_type_id;

    public User() {
    }

    public User(Long id, String username, String password, String firstname, String second_name, String lastname, String mobilenumber, String telefoonnumber, String user_type_id) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.second_name = second_name;
        this.lastname = lastname;
        this.mobilenumber = mobilenumber;
        this.telefoonnumber = telefoonnumber;
//        this.user_type_id = user_type_id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getTelefoonnumber() {
        return telefoonnumber;
    }

    public void setTelefoonnumber(String telefoonnumber) {
        this.telefoonnumber = telefoonnumber;
    }

    public Integer getUser_type_id() {
        return user_type_id;
    }

    public void setUser_type_id(Integer user_type_id) {
        this.user_type_id = user_type_id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", second_name='" + second_name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", mobilenumber='" + mobilenumber + '\'' +
                ", telefoonnumber='" + telefoonnumber + '\'' +
                ", user_type_id='" + user_type_id + '\'' +
                '}';
    }
}
