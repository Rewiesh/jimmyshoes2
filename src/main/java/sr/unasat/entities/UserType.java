package sr.unasat.entities;

import javax.persistence.*;

@Entity
@Table(name = "user_types")
public class UserType {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long user_type_id;
    @Column
    private String name;

    public UserType() {}

    public UserType(Long user_type_id, String name) {
        this.user_type_id = user_type_id;
        this.name = name;
    }

    public UserType(String name) {
        this.user_type_id = user_type_id;
        this.name = name;
    }

    public Long getUser_type_id() {
        return user_type_id;
    }

    public void setUser_type_id(Long user_type_id) {
        this.user_type_id = user_type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserType{" +
                "user_type_id=" + user_type_id +
                ", name='" + name + '\'' +
                '}';
    }
}
