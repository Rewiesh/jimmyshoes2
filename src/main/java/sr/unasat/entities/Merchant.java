package sr.unasat.entities;

import javax.persistence.*;

@Entity
@Table( name = "merchant")
public class Merchant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    public Merchant(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Merchant(String name) {
        this.name = name;
    }

    public Merchant() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Merchant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
